import locale
from decimal import *

# Konfiguration
# Text der zur begrüßung angezeigt wird
GRUSSTEXT = [
    "Willkommen zum Darlehensrechner",
    "Version: 1.1 vom 05.01.2018",
    "Fehler bitte an: itf17a.hallmann@tbs1.nw.lo-net2.de"]
# Größe der Umrandung um den Begrüßungstext
RAHMEN_BREITE = 100
# Maximale Kreditsumme
MAX_KREDIT = 1000000
# Maximale Laufzeit
MAX_LAUFZEIT = 100
# Maximaler Zinssatz
MAX_ZINSSATZ = 100


# einfache Datenklasse
class Kredit:
    # Nur genau diese Attribute zulassen
    # Beim Versuch andere Attribute zur Klasse hinzuzufügen, wird ein Fehler erzeugt
    __slots__ = "summe", "laufzeit", "zinssatz", "art", "gesamt_zinsen", "annuitaet"

    # Konstruktor
    def __init__(self, summe, laufzeit, zinssatz, art):
        self.summe = summe
        self.laufzeit = laufzeit
        self.zinssatz = zinssatz
        self.art = art  # endfällig, oder annuitätisch
        self.gesamt_zinsen = None  # Summe der Zinsen die während der Laufzeit anfallen. Muss erst berechnet werden
        self.annuitaet = None  # nur fürs annuitätische Darlehen


# Zeigt den Begrüßungstext an
def begruessung():
    # Mit * können Strings wiederholt werden
    # Zb. "test" * 3 == "testtesttest"
    print("*" * RAHMEN_BREITE)
    # alle Zeilen der Begrüßung durchgehen und zentrieren
    for zeile in GRUSSTEXT:
        print("**" + zeile.center(RAHMEN_BREITE - 4) + "**")
    print("*" * RAHMEN_BREITE)


# Eingabe vom benutzer abfragen
def kredit_eingabe():
    kreditart = None
    # solange keine gültige Eingabe erfolgt wiederholen
    while kreditart is None:
        user_input = input("Möchten sie ein [e]ndfälliges, oder [a]nnuitätisches Darlehen berechnen? ")
        # hat der benutzer "e", oder "E" für das endfällige Darlehen eingegeben?
        if user_input in ("e", "E"):
            kreditart = "endfaellig"
        elif user_input in ("a", "A"):
            kreditart = "annuitaetisch"
        else:
            # keine gültige Eingabe
            print("Bitte geben sie [e] für ein endfälliges, oder [a] für ein annuitätisches Darlehen ein.")

    kreditsumme = None
    # analog zur eingabe der Kreditart
    while kreditsumme is None:
        # bei eingaben die nicht in Decimal umgewandelt werden können,
        # springt das Programm zum "except"-Teil
        try:
            # schlägt die Umwandlung fehl, wird eine "InvalidOperation"-Ausnahme erzeugt
            # und der Rest des "try"-Blocks wird übersprungen
            user_input = Decimal(input("Gewünschte Kreditsumme in Euro[,Cent]: ")
                                 # Eingaben von "," oder "." als Trennzeichen für Cent erlauben
                                 .replace(",", ".")
                                 # Eingabe von Leerzeichen erlauben, zur besseren Lesbarkeit von großen Beträgen.
                                 # Sie werden einfach entfernt vor der Umwandlung in Decimal
                                 .replace(" ", ""))
            # Eingabe hat keinen Fehler erzeugt. Wurden höchstens zwei Nachkommastellen angegeben?
            # as_tuple() gibt ein benanntes Tupel (sign, digits, exponent) zurück
            if user_input.as_tuple().exponent < -2:
                print("Bitte höchstens 2 Nachkommastellen")
                continue
            # keine unrealistischen Werte für den Kredit erlauben
            if 0 < user_input < MAX_KREDIT:
                kreditsumme = user_input
            else:
                print("Kreditsumme muss positiv und kleiner als 1 Million sein")
        except InvalidOperation:
            print("Falsches Format")

    laufzeit = None
    # analog zur Eingabe der Kreditsumme
    while laufzeit is None:
        try:
            # Eingabe muss ganze Zahl sein, sonst wird ein "ValueError" erzeugt
            # und das Programm springt zum "except"-Teil
            user_input = int(input("Laufzeit in Jahren: "))
            if 0 < user_input <= MAX_LAUFZEIT:
                laufzeit = user_input
            else:
                print("Laufzeit muss zwischen 1 und 100 Jahren liegen")
        except ValueError:
            print("Bitte ganze Zahl eingeben")

    zinssatz = None
    # nichts neues, analog wie oben
    while zinssatz is None:
        try:
            user_input = Decimal(input("Zinssatz in %: ").replace(",", "."))
            if 0 < user_input <= MAX_ZINSSATZ:
                zinssatz = user_input / 100
            else:
                print("Zinssatz sollte zwischen 0 und 100% liegen")
        except InvalidOperation:
            print('Bitte Zahl eingeben.')
    return Kredit(kreditsumme, laufzeit, zinssatz, kreditart)


# berechnet die Zinsen eines endfälligen Kredits
def berechne_endfaellig(_kredit):
    _kredit.gesamt_zinsen = _kredit.summe * _kredit.laufzeit * _kredit.zinssatz


# berechnet die Zinsen eines annuitätischen Kredits
def berechne_annuitaetisch(_kredit):
    # Variablennamen angepasst an
    # https://de.wikipedia.org/wiki/Annuit%C3%A4tendarlehen
    # Abschnitt "Weitere Formeln" -> "Summe der zu leistenden Zinszahlungen"
    s = _kredit.summe
    n = _kredit.laufzeit
    i = _kredit.zinssatz
    q = i + 1
    # q**n bedeutet q hoch n in Python
    _kredit.gesamt_zinsen = s * (n * ((q - 1) / (q**n - 1)) * q**n - 1)


def berechne_annuitaet(_kredit):
    # Variablennamen angepasst an
    # https://de.wikipedia.org/wiki/Annuit%C3%A4tendarlehen
    # Abschnitt "Weitere Formeln" -> "Summe der zu leistenden Zinszahlungen"
    s = _kredit.summe
    n = _kredit.laufzeit
    i = _kredit.zinssatz
    q = i + 1
    # q**n bedeutet q hoch n in Python
    _kredit.annuitaet = s * ((q**n * i)/(q**n - 1))


# gibt die Ergebnisse der Berechnung aus
def ausgabe(_kredit):
    print("\n\nBei einer Kreditsumme von {} über eine Laufzeit von {} Jahren\n"
          "mit einem Zinssatz von {:g}% fallen {} Zinsen an."
          # locale.currency(wert) gibt einen Wert als Währung formatiert zurück
          # grouping=True erzeugt Tausender Trennzeichen bei der Ausgabe
          .format(locale.currency(_kredit.summe, grouping=True), _kredit.laufzeit,
                  # den Zinssatz erst in float Umzuwandeln und dann mit
                  # {:g} zu formatieren ist umständlich,
                  # aber es ist die Einzige möglichkeit die ich gefunden habe,
                  # um überflüssige Nullen am Ende zu entfernen.
                  # Decimal.normalize() entfernt zwar auch überflüssige Nullen,
                  # wandelt aber zB. 10 in 1E+1 um...
                  float(_kredit.zinssatz * 100), locale.currency(_kredit.gesamt_zinsen, grouping=True)))
    if _kredit.art == "annuitaetisch":
        print("Die jährliche Annuität beträgt " + locale.currency(_kredit.annuitaet, grouping=True))


# Main-Methode
if __name__ == '__main__':

    # Initialisierung
    # Formatierung benutzt deutsche Zeichen,
    # zB. € als Einheit, oder "." als Tausender Trennzeichen für locale.currency()
    try:
        locale.setlocale(locale.LC_ALL, 'german')
    except locale.Error:
        # läuft unter Linux nur so:
        locale.setlocale(locale.LC_ALL, 'de_DE.utf-8')
    begruessung()

    # schleife bis Programmabbruch
    weitereEingabe = True
    while weitereEingabe:
        # Eingabe
        kredit = kredit_eingabe()

        # Berechnung
        if kredit.art == "endfaellig":
            berechne_endfaellig(kredit)
        else:
            berechne_annuitaetisch(kredit)
            berechne_annuitaet(kredit)

        # Ausgabe
        ausgabe(kredit)

    input("Weitere berechnung? (j/n)")
    weitereEingabe = None
    # solange keine gültige Eingabe erfolgt wiederholen
    while weitereEingabe is None:
        user_input = input("Weitere berechnung? [j/n] ")
        if "j" == user_input:
            weitereEingabe = True
        elif "n" == user_input:
            weitereEingabe = False
        else:
            print("Bitte geben sie [j] für eine weitere Berechnung, oder [n] für den Programmabbruch ein.")
