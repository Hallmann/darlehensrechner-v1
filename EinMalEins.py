if __name__ == '__main__':

    columnCaptions = ("{:>4}"*12).format("*", "|", *range(10))
    print(columnCaptions)
    print("-"*len(columnCaptions))

    for a in range(10):
        print(("{:>4}" * 12).format(a, "|", *[a*b for b in range(10)]))
