if __name__ == '__main__':

    # checking input is longer than the program... never expect users to do what they should
    howManyNumbers = None
    while howManyNumbers is None:
        try:
            howManyNumbers = int(input("Wie viele Zahlen?: "))
            if howManyNumbers < 1:
                howManyNumbers = None
        except ValueError:
            print("Positive Zahl bitte")

    # the real program
    last, current = 0, 1
    for i in range(howManyNumbers):
        print(current)
        last, current = current, last + current
